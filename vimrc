" Use :so % to reload

" force me to set everything explicitly
let skip_defaults_vim=1

set nocompatible

set background=dark
colorscheme elflord
" colorscheme pablo

set title
set titlestring=%t\ %m\ (%{expand('%:p:h')})


" line numbers
set number " changed from set nonumber on 6/26/2020
set relativenumber
set ruler

" turns tabs into spaces
set expandtab " use softtabstop spaces instead of a tab character
set tabstop=2 
set softtabstop=2 " indent by 2 spaces when pressing <TAB>
set shiftwidth=2 " indent by 2 spaces when using >>, <<, etc
set smartindent " automatically inserts indentation in some cases
set smarttab
set autoindent " keep indentation from previous line

" wordwrap
set textwidth=0 " changed from set textwidth=73 on 6/26/2020
" set textwidth=0 will cause vim to not enter line break
set linebreak

set listchars=tab:→\ ,eol:↲,nbsp:␣,space:·,trail:·,extends:⟩,precedes:⟨

" no backup but doesn't leave temp/swap files around
set nobackup
set nowritebackup
set noswapfile

" turns off the annoying audio bell when i make a mistake
set belloff=all

" sync terminal title with current file edited
set icon

" keeps cursor in the middle of the screen
set scrolloff=999

" highlight search, type :noh to turn it off, type n to go to next search
" item, type N to go to previous item
set hlsearch
set incsearch

" prevents truncated yanks, deletes, etc.
" viminfo is how your cut/paste persists across files
set viminfo='20,<1000,s1000 

" no bracket matching
let g:loaded_matchparen=1
set noshowmatch

" not a fan of bracket matching nor folding (zf/zd
set foldmethod=manual

" if you are using a Mac required for delete to work
if $PLATFORM == 'MAC'
  set backspace=indent,eol,start
endif

" stop complaints about switching buffer with changes
set hidden

set history=100

syntax enable

" allow sensing the filetype
filetype plugin on

" Install vim-plug if not already installed
" if empty(glob('~/.vim/autoload/plug.vim'))
"  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
"    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"  autocmd VimEnter * PlugInstall
" endif

" read personal/private vim configuration (keep this last to override)
" set rtp^=~/.vimpersonal
" set rtp^=~/.vimprivate
