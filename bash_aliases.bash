# this lets you use Vi commands on the bash command line.
# hit ESC or Ctrl-[ to get into command mode, type 'i' to go back to
# typing
set -o vi

# aliases
# unalias -a
alias ll="ls -lF"
alias la="ls -lAF"
alias l="ls -CF"
alias vi=vim
export PATH=$HOME/repos/gitlab.com/v3ylw/dotfiles/scripts:$PATH
