(define (define-mouse-chords chord-key . definitions)
  (define (start-mouse-chord)
    (let ((cmd #f))
      (for-each
        (lambda (definition)
          (let ((key (list-ref definition 0)) (binding (list-ref definition 1)))
            (xbindkey-function key (lambda () (set! cmd binding)))))
        definitions)
      (xbindkey-function `(release ,chord-key)
        (lambda ()
          (remove-xbindkey `(release ,chord-key))
          (for-each
            (lambda (definition)
              (let ((key (list-ref definition 0)))
                (remove-xbindkey key)))
            definitions)
          (if cmd
            (begin
              (run-command cmd)
              (set! cmd #f))
          )))))
  (xbindkey-function chord-key start-mouse-chord))

(define-mouse-chords "b:10"
  (list '(release "b:1") "xte 'keydown Control_L' 'keydown Shift_L' 'key Tab' 'keyup Control_L' 'keyup Shift_L'")
  (list '(release "b:2") "xte 'keydown Control_L' 'key Tab' 'keyup Control_L'")
  (list '(release "b:3") "xte 'keydown Control_L' 'key w' 'keyup Control_L'")
)
